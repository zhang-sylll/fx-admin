import { createRouter, createWebHistory } from 'vue-router'
import { ROLE_TYPE } from '@/constants'
import system from './system-setup'

/**
 *  title: 标题,
 *  keepAlive: 缓存,
 *  authedRoles: 授权的角色
 */

const DefaultLayout = () => import('@/layout/default.vue')

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    ...system,
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login/index.vue')
    },
    {
      path: '',
      redirect: '/login',
      component: DefaultLayout,
      children: [
        {
          path: '/basic-setting',
          name: 'basic-setting',
          component: () => import('@/views/basic-setting/index.vue'),
          meta: {
            title: '一级代表团',
            keepAlive: true,
            authedRoles: [ROLE_TYPE.sponsor]
          }
        }
      ]
    },
    {
      path: '/:catchAll(.*)',
      component: () => import('@/layout/404.vue')
    }
  ]
})

export default router
