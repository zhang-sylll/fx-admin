const DefaultLayout = () => import('@/layout/default.vue')

export default [
  {
    path: '/system',
    component: DefaultLayout,
    meta: {
      activeMenu: '/system',
      title: '系统设置'
    },
    children: [
      {
        path: 'permission',
        component: () =>
          import('@/views/system-setup/permission-manage/index.vue'),
        meta: {
          activeMenu: '/system/permission',
          title: '权限管理',
          keepAlive: true
        }
      },
      {
        path: 'permission-detail',
        component: () =>
          import('@/views/system-setup/permission-manage/detail.vue'),
        meta: {
          activeMenu: '/system/permission',
          title: '权限管理'
        }
      },
      {
        path: 'account',
        component: () =>
          import('@/views/system-setup/account-manage/index.vue'),
        meta: {
          activeMenu: '/system/account',
          title: '账号管理',
          keepAlive: true
        }
      },
      {
        path: 'account-detail',
        component: () =>
          import('@/views/system-setup/account-manage/detail.vue'),
        meta: {
          activeMenu: '/system/account',
          title: '账号管理'
        }
      },
      {
        path: 'operationLog',
        component: () =>
          import('@/views/system-setup/operationLog-manage/index.vue'),
        meta: {
          activeMenu: '/system/operationLog',
          title: '操作日志'
        }
      }
      // {
      //   path: 'device',
      //   component: () =>
      //     import('@/views/system-setup/device-manage/index.vue'),
      //   meta: {
      //     activeMenu: '/system/device',
      //     title: '设备管理',
      //   }
      // },
      // {
      //   path: 'device/detail/:id?',
      //   component: () =>
      //     import('@/views/system-setup/device-manage/detail/index.vue'),
      //   meta: {
      //     activeMenu: '/system/device',
      //     title: '设备管理'
      //   }
      // },
      // {
      //   path: 'permission/detail/:id?',
      //   component: () =>
      //     import('@/views/system-setup/permission-manage/detail/index.vue'),
      //   meta: {
      //     activeMenu: '/system/permission',
      //     title: '权限管理'
      //   }
      // },

      // {
      //   path: 'hall',
      //   component: () => import('@/views/system-setup/hall-manage/index.vue'),
      //   meta: {
      //     activeMenu: '/system/hall',
      //     title: '展馆管理'
      //   }
      // },
      // {
      //   path: 'hall/detail/:id?',
      //   component: () =>
      //     import('@/views/system-setup/hall-manage/detail/index.vue'),
      //   meta: {
      //     activeMenu: '/system/hall',
      //     title: '展馆管理'
      //   }
      // },
      // {
      //   path: 'notice',
      //   component: () =>
      //     import('@/views/system-setup/notice-manage/index.vue'),
      //   meta: {
      //     activeMenu: '/system/notice',
      //     title: '全局通知',
      //   }
      // },
      // {
      //   path: 'notice/detail/:id?',
      //   component: () =>
      //     import('@/views/system-setup/notice-manage/detail/index.vue'),
      //   meta: {
      //     activeMenu: '/system/notice',
      //     title: '通知管理'
      //   }
      // },
    ]
  }
]
