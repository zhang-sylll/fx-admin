import { computed, ref } from 'vue'
import { useLocalStorage } from '@vueuse/core'

// TODO 逻辑没写完
export function useLangs() {
  const cacheLang = useLocalStorage('de-locale', 'zh-cn')

  const lang = ref(cache.value)

  /**
   * @param {'zh-cn'|'zh-tw'|'en'} locale
   */
  const setLang = locale => {
    lang.value = locale
    cacheLang.value = locale
  }

  return {
    lang: computed(() => lang.value),
    setLang
  }
}
