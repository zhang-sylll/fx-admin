export const LOCALES = {
  /**
   * 简体中文
   */
  zhCn: 'zh-cn',
  /**
   * 繁体中文
   */
  zhTw: 'zh-tw',
  /**
   * 英文
   */
  en: 'en'
}

export const localeLabels = {
  [LOCALES.zhCn]: '简体中文',
  [LOCALES.zhTw]: '繁體中文',
  [LOCALES.en]: 'English'
}
