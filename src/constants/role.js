/**
 * 角色类型
 */
export const ROLE_TYPE = {
  /**
   * 超管
   */
  admin: 1,
  /**
   * 主办
   */
  sponsor: 2,
  /**
   * 团组
   */
  group: 9999
}
