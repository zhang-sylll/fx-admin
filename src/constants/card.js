/**
 * 身份证件类型
 */
export const ID_CARD_TYPE = [
  {
    label: '身份证',
    value: 1
  },
  {
    label: '护照',
    value: 2
  },
  {
    label: '军官证',
    value: 3
  },
  {
    label: '台胞证',
    value: 4
  },
  {
    label: '回乡证（港澳居民来往内地通行证）',
    value: 5
  }
]

/**
 * 证件审核状态
 */
export const CARD_AUDIT_STATUS = [
  {
    label: '未审核',
    labelZhTw: '未审核',
    labelEn: '未审核',
    value: 0,
    className: 'info'
  },
  {
    label: '已审核',
    labelZhTw: '已审核',
    labelEn: '已审核',
    value: 1,
    className: 'success'
  },
  {
    label: '已删除',
    labelZhTw: '已删除',
    labelEn: '已删除',
    value: 2,
    className: 'danger'
  },
  {
    label: '已驳回',
    labelZhTw: '已驳回',
    labelEn: '已驳回',
    value: 3,
    className: 'danger'
  },
  {
    label: '待审核',
    labelZhTw: '待审核',
    labelEn: '待审核',
    value: 4,
    className: 'info'
  },
  {
    label: '已打印',
    labelZhTw: '已打印',
    labelEn: '已打印',
    value: 5,
    className: 'success'
  },
  {
    label: '已发放',
    labelZhTw: '已发放',
    labelEn: '已发放',
    value: 6,
    className: 'success'
  }
]
