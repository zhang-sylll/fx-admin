import { service } from '@/utils'

/** 分页查询证件 */
export function getCardTypes(params) {
  return service.get('/cardType/selectCardTypeList', { params })
}

/** 新增证件类型 */
export function addCardType(params) {
  return service.post('/cardType/addCardType', params)
}

/** 删除证件类型||真删除 */
export function deleteCardType(params) {
  return service.get('/cardType/deleteCardType', { params })
}

/** 编辑证件类型 */
export function editCardType(params) {
  return service.post('/cardType/updateCardType', params)
}
