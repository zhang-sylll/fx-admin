import { service } from '@/utils'

/** 数据统计：查询日申请证件 */
export function getAddCardOfDay(params) {
  return service.post('/userInfo/getAddCardOfDay', params)
}

/** 数据统计：查询某证件-团组证件数量 */
export function getGroupExpoUserNumOfCount(params) {
  return service.get('/userInfo/getGroupExpoUserNumOfCount', { params })
}

/** 证件数量查询 */
export function getCardsNum(params) {
  return service.get('/userInfo/selectExpoUserNum', { params })
}

/** 分页查询团组下证件的数量 */
export function getCardNumByGroup(params) {
  return service.get('/userInfo/selectGroupUserNum', { params })
}

/** 查询入场人次 */
export function getVisitorsNum(params) {
  return service.get('/entranceStatistical/querySharingNum', { params })
}

/** 数据统计：查询证件类型观众各地点入场数 */
export function getCardTypeUserEnterOfDay(params) {
  return service.post('/userInfo/getCardTypeUserEnterOfDay', params)
}
