import { service } from '@/utils'

/** 图片上传接口type:上传文件的路径 1:{project} 2:{exhibor} 3:{small} 4:{builder} 5:{home} 6:{dynamic} */
export function uploadImage(params) {
  return service.post('/ltupload/images', params, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}
