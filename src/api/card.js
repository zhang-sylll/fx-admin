import { service } from '@/utils'

/** 证件分页查询 */
export function getCards(params) {
  return service.get('/userInfo/selectuserInfo', { params })
}

/** 新增报名用户 */
export function applyCard(params) {
  return service.post('/userInfo/addExpoUser', params)
}

/** 证件单条查询 */
export function getCardById(params) {
  return service.get('/userInfo/selectUserInfoId', { params })
}

/** 编辑证件 */
export function editCard(params) {
  return service.post('/userInfo/updateUserInfo', params)
}

/** 删除证件 */
export function deleteCard(params) {
  return service.post('/userInfo/deleteUser', params)
}

/** 批量操作证件 */
export function operateBatchCard(params) {
  return service.post('/userInfo/batchDealExpoUser', params)
}

/** 审核驳回euid */
export function rejectCard(params) {
  return service.post('/userInfo/ReviewRejected', params)
}

/** 审核通过euid */
export function approvedCard(params) {
  return service.post('/userInfo/approved', params)
}

/** 通过团组查询证件 */
export function getCardTypesByGroupId(params) {
  return service.get('/userInfo/selectGroupCardList', { params })
}

/** 人像上传 */
export function uploadBatchPhotos(params) {
  return service.post('/userInfo/idImage', params, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

/** 查询,导入历史,分页查询 */
export function getRecordByCondition(params) {
  return service.get('/userInfo/import/getRecordByCondition', { params })
}

/** 导入,证件数据（2：参展商，3：贵宾含VIP，4：媒体观众，5：工作人员，6：施工人员） */
export function importCard(params) {
  return service.post('/userInfo/import/credentials', params, {
    headers: { 'Content-Type': 'multipart/form-data' }
  })
}

/** 批量修改证件的证件类型 */
export function batchModifyCardType(params) {
  return service.post('/userInfo/batchModifyCardType', params)
}
