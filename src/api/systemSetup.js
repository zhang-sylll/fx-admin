import { service } from '@/utils'

export default {
  selectSysLogList: params =>
    service.get('/sysLog/selectSysLogList', { params }),
  addNotice: params => service.post('/admin/notice/addNotice', params),
  updateNotice: params => service.post('/admin/notice/updateNotice', params),
  selectNoticeId: params =>
    service.get('/admin/notice/selectNoticeId', { params }),
  pageNoticeList: params =>
    service.get('/admin/notice/pageNoticeList', { params }),
  deleteNoticeId: params =>
    service.get('/admin/notice/deleteNoticeId', { params }),
  // 创建新角色并绑定菜单
  addRoleAndBindingMenu: params =>
    service.post('/admin/sysPermissions/addRoleAndBindingMenu', params),

  // 删除角色
  deleteRoleById: params =>
    service.delete(`/admin/sysPermissions/deleteRoleById/${params.id}`),

  // 修改角色绑定的菜单
  updateRoleBindingMenu: params =>
    service.post('/admin/sysPermissions/updateRoleBindingMenu', params),

  // 查询角色列表
  getRoleList: params =>
    service.get('/admin/sysPermissions/getRoleList', { params }),

  // 根据角色ID查询该角色拥有的菜单
  findPermissionsByRoles: params =>
    service.get('/admin/sysPermissions/findPermissionsByRoles', { params }),

  // 根据角色id查询角色信息
  querySysRolesById: params =>
    service.get('/admin/sysPermissions/querySysRolesById', { params }),

  // 添加管理员用户
  addSysUser: params => service.post('/admin/user/addSysUser', params),

  // 删除管理员用户
  deleteBypassAccount: params =>
    service.get(`/admin/user/deleteBypassAccount?id=${params.id}`),

  // 修改管理员用户
  updateSysUser: params => service.post('/admin/user/updateSysUser', params),

  // 查询管理员用户列表
  querySysUserList: params =>
    service.get('/admin/user/querySysUserList', { params }),

  // 查询管理员用户详情
  querySysUser: params => service.get('/admin/user/querySysUser', { params }),

  // 查询所有角色
  getHomeAllRole: params =>
    service.get('/admin/user/getManageAllRole', { params }),

  // 启用管理员用户
  startBypassAccount: params =>
    service.get(`/admin/user/startBypassAccount?id=${params.id}`),

  // 禁用管理员用户
  disableBypassAccount: params =>
    service.get(`/admin/user/disableBypassAccount?id=${params.id}`),

  // 添加展馆
  addPavilion: params =>
    service.post('/admin/homePavilion/addPavilion', params),

  // 删除展馆
  deletePavilion: params =>
    service.post(`/admin/homePavilion/deletePavilion?id=${params.id}`),

  // 修改展馆
  updatePavilion: params =>
    service.post('/admin/homePavilion/updatePavilion', params),

  // 查询展馆列表
  queryPavilionList: params =>
    service.get('/admin/homePavilion/queryPavilionList', { params }),

  // 查询展馆详情
  queryPavilionDetail: params =>
    service.get('/admin/homePavilion/queryPavilionDetail', { params }),

  // 重置管理员用户密码
  resetSysUserPassword: params =>
    service.get('/admin/user/resetSysUserPassword', { params }),
  // 删除设备
  deleteDevice: params => service.get('/device/deleteDevice', { params }),
  // 加载设备列表
  findList: params => service.get('/device/findList', { params }),
  // 通过id查询设备
  queryDeviceId: params => service.get('/device/queryDeviceId', { params }),
  // 新增设备
  insertDv: params => service.post('/device/insertDv', params),
  // 修改设备
  updateDeviceId: params => service.post('/device/updateDeviceId', params)
}
