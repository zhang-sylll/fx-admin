import { service } from '@/utils'

/** 查询日志列表 */
export function getSystemLog(params) {
  return service.get('/sysLog/selectSysLogList', { params })
}
