import { service } from '@/utils'

export function login(params) {
  return service.post('/sysLogin/login', params)
}
export function getUser(params) {
  return service.get('/sysLogin/getUser', params)
}
export function getRoleMenus(params) {
  return service.get('/admin/sysPermissions/findPermissionsByRoles', params)
}
export function getGraphicsCode(params) {
  return service.get('/sms/getGraphicsCode', { params })
}
