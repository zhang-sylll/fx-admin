import { service } from '@/utils'

/** 注册团体账号 */
export function createGroup(params) {
  return service.post('/groups/insertGroupSysLogin', params)
}

/** 团体列表分页查询 */
export function getGroups(params) {
  return service.get('/groups/queryGroups', { params })
}

/** 团体id查询 */
export function getGroupsById(params) {
  return service.get('/groups/queryGroupId', { params })
}

/** 编辑团体 */
export function editGroup(params) {
  return service.post('/groups/updateGroups', params)
}

/** 重置团体账号密码 */
export function resetGroupPassword(params) {
  return service.get('/groups/resetGroupPassword', { params })
}

/** 审核||state为1通过；state为2驳回，驳回需要传理由 */
export function deleteGroup(params) {
  return service.post('/groups/audit', params)
}

/** 团体列表查询 */
export function getGroupsNotPageable(params) {
  return service.get('/groups/queryGroupsList', { params })
}
