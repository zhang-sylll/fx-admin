/**
 * @param {string} val 身份证号码
 * @returns {boolean}
 */
export function isIdCard(val) {
  if (!/^\d{17}(\d|x)$/i.test(val)) {
    return false
  }
  val = val.replace(/x$/i, 'a')

  if (chinaCity[parseInt(val.substr(0, 2))] == null) {
    // 非法地区
    return false
  }
  const sBirthday =
    val.substr(6, 4) +
    '-' +
    Number(val.substr(10, 2)) +
    '-' +
    Number(val.substr(12, 2))

  const d = new Date(sBirthday.replace(/-/g, '/'))

  if (
    sBirthday !=
    d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate()
  ) {
    // 非法生日
    return false
  }

  let iSum = 0
  for (let i = 17; i >= 0; i--) {
    iSum += (Math.pow(2, i) % 11) * parseInt(val.charAt(17 - i), 11)
  }
  if (iSum % 11 != 1) {
    // 非法证号
    return false
  }
  // return chinaCity[parseInt(val.substr(0, 2))] + "," + sBirthday + "," + (val.substr(16, 1) % 2 ? "男" : "女")
  return true
}

export const chinaCity = {
  11: '北京',
  12: '天津',
  13: '河北',
  14: '山西',
  15: '内蒙古',
  21: '辽宁',
  22: '吉林',
  23: '黑龙江',
  31: '上海',
  32: '江苏',
  33: '浙江',
  34: '安徽',
  35: '福建',
  36: '江西',
  37: '山东',
  41: '河南',
  42: '湖北',
  43: '湖南',
  44: '广东',
  45: '广西',
  46: '海南',
  50: '重庆',
  51: '四川',
  52: '贵州',
  53: '云南',
  54: '西藏',
  61: '陕西',
  62: '甘肃',
  63: '青海',
  64: '宁夏',
  65: '新疆',
  71: '台湾',
  81: '香港',
  82: '澳门',
  91: '国外'
}
