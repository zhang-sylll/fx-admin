import { LOCALES } from '@/constants'

export function getLocale() {
  return localStorage.getItem('de-locale') || 'zh-cn'
}

/**
 *
 * @param {'zh-cn'|'zh-tw'|'en'} locale
 */
export function setLocale(locale) {
  if (!Object.values(LOCALES).includes(locale)) {
    throw new Error(
      `无效locale， 支持的选项有： ${Object.values(LOCALES).join('、')}`
    )
  }
  localStorage.setItem('de-locale', locale)
}
