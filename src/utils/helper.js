import { unref } from 'vue'
import { ElMessage } from 'element-plus'

export async function validateSelected(data, options = {}) {
  const { isShowErrorTips = true, prop = 'id' } = options

  const unrefedData = unref(data)
  if (unrefedData?.length) {
    return unrefedData.map(item => item[prop])
  } else {
    isShowErrorTips && ElMessage.warning('请选择数据再进行操作')
    return Promise.reject(false)
  }
}
