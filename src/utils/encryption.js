import { JSEncrypt } from 'jsencrypt' //RSA加密
// 密钥对生成 http://web.chacuo.net/netrsakeypair;
// 把下面生成的公钥、私钥换成自己生成的即可

const pubKey =
  'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDoCctgW6inUA45vDQjVd88YDGFrmt4KZ10+219no8bjaNxERBnkwIPshgzOegfIY2eay56T2J83gUk+OZNWZHHWoKrBWb9kU2j/LxC7TqNF2EHbSk/Z12W0I5/qDsqRw6sMzE4T1wXvGsY62lLq3XlhRrji3ae00tYOdxobpOa5wIDAQAB'

// RSA加密
export function EnRsa(data) {
  //new一个对象
  const encrypt = new JSEncrypt()
  //设置公钥
  encrypt.setPublicKey(pubKey)
  //password是要加密的数据,此处不用注意+号,因为rsa自己本身已经base64转码了,不存在+,全部是二进制数据
  const result = encrypt.encrypt(data)
  return result
}

// RSA解密 （未知私钥）
export function DeRsa(privateKey, data) {
  // 新建JSEncrypt对象
  const decrypt = new JSEncrypt()
  // 设置私钥
  decrypt.setPrivateKey(privateKey)
  // 解密数据
  const result = decrypt.decrypt(data)
  return result
}
