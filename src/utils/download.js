import { ElNotification } from 'element-plus'
import { service } from './service'
import { isBlob } from '@/utils'
import { isObject } from '@vueuse/core'
import { i18n } from '@/plugins/i18n'

export async function downloadFile(option) {
  const { url, method = 'get', data = {} } = option
  const t = i18n.global.t

  const notificationInstance = ElNotification.info({
    title: t('download.operateSuccess'),
    message: t('download.exporting'),
    duration: 0
  })
  // notificationInstance = ElNotification.info({
  //   title: '正在导出',
  //   message: `已导出${aaa.value}%, 请稍候...`,
  //   duration: 0
  // })

  const config = {
    responseType: 'blob',
    onDownloadProgress: progressEvent => {
      const {
        total: totalBytes,
        loaded: downloadedBytes,
        progress
      } = progressEvent
      console.log('axios download progress : ', progress)

      if (totalBytes) {
        const downloadPercentage = Math.round(
          (downloadedBytes / totalBytes) * 100
        )
        console.log('downloadPercentage: ', downloadPercentage, '%')
      } else {
        console.log('无法获取文件大小, 无法显示下载进度')
      }
    }
  }

  let request
  switch (method) {
    case 'get':
      config.params = data
      request = () => service[method](url, config)
      break
    case 'post':
      request = () => service[method](url, data, config)
      break
    default:
      throw new Error(`method参数异常, 无法执行${method}请求`)
  }

  try {
    const { data: result, headers } = await request()
    if (isBlob(result)) {
      handleDownload(result, getFileNameByResponseHeader(headers))
      ElNotification.success({
        title: t('download.success'),
        message: t('download.successPrompt')
      })
    } else {
      handleResponseError(result)
    }
  } catch (error) {
    ElNotification.error({
      title: t('download.error'),
      message: t('download.errorPrompt')
    })
    console.error('download error : ', error)
  } finally {
    notificationInstance.close()
  }
}

/**
 * @param {object} headers
 *
 * 从响应头中获取文件名
 */
function getFileNameByResponseHeader(headers) {
  return (
    decodeURI(headers['content-disposition']?.split('=')[1]) ||
    (+new Date()).toString()
  )
}

/**
 * @param {Blob} blob
 * @param {string} fimename
 *
 * 下载二进制文件
 */
export function handleDownload(blob, fimename) {
  const downloadUrl = window.URL.createObjectURL(blob)

  const link = document.createElement('a')
  link.href = downloadUrl
  link.setAttribute('download', fimename)
  const clickEvent = new MouseEvent('click', {
    view: window,
    bubbles: true,
    cancelable: false
  })

  link.dispatchEvent(clickEvent)

  window.URL.revokeObjectURL(downloadUrl)
}

function handleResponseError(data) {
  if (isObject(data) && 'code' in data && 'msg' in data) {
    ElNotification.error({
      title: t('download.error'),
      message: `${data.code} : ${data.msg}`
    })
  } else {
    ElNotification.error({
      title: t('download.error'),
      message: t('download.unknownException')
    })
  }
}
