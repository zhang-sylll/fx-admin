export const NOOP = _ => _

export const extend = Object.assign

export const hasOwn = Object.hasOwn

export const objectToString = Object.prototype.toString

export const isFunction = val => typeof val === 'function'
export const isNumber = val => typeof val === 'number'

export const isBlob = val => objectToString.call(val) === '[object Blob]'

export const isArray = Array.isArray
