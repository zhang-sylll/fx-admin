/** 金额, 最多保留两位小数 */
export const MONEY_REG = /^(([1-9]{1}\d*)|(0{1}))(\.\d{1,2})?$/

/** 字母 */
export const LETTER_REG = /^[a-zA-Z]+$/

/** 字母数字下划线 */
export const LETTER_OR_NUMBER_OR___REG = /^[a-zA-Z\d_]+$/

/** 手机 */
export const PHONE_REG = /^1[2|3|4|5|6|7|8|9][0-9]\d{8}$/

// 数字|英文
export const NUMBER_OR_ENGLISH_REG = /^[a-zA-Z0-9]+$/
