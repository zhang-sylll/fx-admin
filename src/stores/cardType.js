import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useUserStore } from './user'
import { userApi } from '@/api'
import { useRoute } from 'vue-router'

export const useCardTypeStore = defineStore('cardType', () => {
  const userStore = useUserStore()
  const cardTypes = ref([])
  const getCardTypes = async () => {
    const { data } = await userApi.getCardTypeByUser({
      expoId: userStore.expoId,
      groupId: userStore.isSponsor ? 0 : userStore.groupId
    })
    return (cardTypes.value = data)
  }

  const route = useRoute()
  const activeCardType = computed(() =>
    cardTypes.value.find(
      cardType => cardType.id === Number(route.params.cardType)
    )
  )
  return {
    getCardTypes,
    cardTypes: computed(() => cardTypes.value),
    activeCardType,
    activeCardTypeName: computed(() => activeCardType.value?.cardName)
  }
})
