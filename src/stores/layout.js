import { ref, watchEffect } from 'vue'
import { defineStore } from 'pinia'
import { useWindowSize } from '@vueuse/core'

export const useLayoutStore = defineStore('layout', () => {
  const isCollapse = ref(false)

  const changeCollapse = collapse => {
    isCollapse.value = collapse ?? !isCollapse.value
  }

  const md = 1024
  const { width } = useWindowSize()
  let prevWidth = width.value
  watchEffect(() => {
    const isMobile = width.value < md
    if (isMobile && prevWidth > width.value && !isCollapse.value) {
      changeCollapse(true)
    } else if (prevWidth < width.value && !isMobile) {
      changeCollapse(false)
    }
    prevWidth = width.value
  })

  return { isCollapse, changeCollapse }
})
