import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { LOCALES } from '@/constants'

export const useLocalesStore = defineStore('locales', () => {
  const locales = ref(LOCALES.zhCn)

  const setLocales = locales => {
    locales.value = locales
  }

  return { locales, setLocales }
})
