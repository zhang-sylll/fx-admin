import { createI18n } from 'vue-i18n'
import { LOCALES } from '@/constants'
import { getLocale } from '@/utils'
import zhCn from '@/locales/zh-cn'
import zhTw from '@/locales/zh-tw'
import en from '@/locales/en'

/**
 * @see @link https://vue-i18n.intlify.dev/guide/
 */
const i18n = createI18n({
  locale: getLocale(),
  fallbackLocale: LOCALES.zhCn,
  allowComposition: true, // you need to specify that!
  messages: {
    [LOCALES.zhCn]: zhCn,
    [LOCALES.zhTw]: zhTw,
    [LOCALES.en]: en
  }
})

export { i18n }
