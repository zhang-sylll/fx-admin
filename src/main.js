import 'element-plus/dist/index.css' // element 样式
import 'element-plus/theme-chalk/dark/css-vars.css'
import './assets/css/tailwind.css'
import './assets/css/element-plus.css'
import './assets/css/scroll.css'
import './assets/css/app.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { i18n } from './plugins/i18n'

import App from './App.vue'
import router from './router'
import './permission.js'

const app = createApp(App)

app.use(i18n)
app.use(createPinia())
app.use(router)

app.mount('#app')
