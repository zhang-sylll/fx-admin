export const download = {
  success: '導出成功',
  successPrompt: '可在瀏覽器下載任務中查看',
  error: '導出失敗',
  errorPrompt: '請重試或與管理員聯系',
  operateSuccess: '操作成功',
  exporting: '正在導出，請稍候...',
  unknownException: '未知異常, 請截圖與管理員聯系'
}
