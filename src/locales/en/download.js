export const download = {
  success: 'Export successful',
  successPrompt: 'Can be viewed in browser download tasks',
  error: 'Export failed',
  errorPrompt: 'Please try again or contact the administrator',
  operateSuccess: 'Operation successful',
  exporting: 'Exporting, please wait',
  unknownException:
    'Unknown exception, please take a screenshot to contact the administrator'
}
