import { download } from './download'

export default {
  download,
  group: {
    refresh: 'Refresh',
    delegation: 'Delegation',
    contactName: 'Contact name',
    contactNumber: 'Contact number',
    account: 'Account',
    password: 'Password',
    belongsDelegation: 'Belongs to Delegation',
    firstLevelDelegation: 'First-level delegation',
    secondLevelDelegation: 'Second-level delegation'
  },
  global: {
    delegationManagement: 'Delegation Management',
    exportTooltip:
      'The export conditions are the same as those for querying the list',
    validUploadLimit:
      'Upload failed, the maximum number of files is {limit}, and {uploadedLength} files have been uploaded',
    vaildUploadSize:
      'Upload failed, the maximum file size is {size} MB, and the selected file is {userSize} MB',
    vaildUploadType: 'The system currently only supports uploading images',
    uploadTip: '{type} files with a size less than {size}MB',
    operation: 'Operation',
    edit: 'edit',
    delete: 'delete',
    search: 'query',
    create: 'Add',
    export: 'export',
    import: 'Import',
    index: 'Index',
    dargFileHereOr: 'Drag and drop files here or',
    clickToUpload: 'Click to upload',
    file: 'File',
    logOut: 'Log out',
    deleteTips: 'Delete {tips}, do you want to continue',
    tips: 'Prompt',
    confirm: 'Confirm',
    cancel: 'Cancel',
    man: 'male',
    woman: 'female',
    gender: 'Gender',
    pleaseInput: 'Please enter {label}',
    pleaseInputCorrect: 'Please enter the correct {label}',
    pleaseSelect: 'Please select {label}',
    pleaseUpload: 'Please upload {label}',
    apply: 'Application',
    myApply: 'My application',
    cardApply: 'Document application'
  },
  login: {
    login: 'Login',
    account: 'Account',
    accountPlaceholder: 'Please enter an account',
    password: 'password',
    passwordPlaceholder: 'Please enter the password'
  },
  cardApply: {
    myApply: {
      cardInfo: 'Document information',
      pageTitle: 'My Application List',
      name: 'Name',
      phone: 'Phone',
      cardType: 'Document type ',
      company: 'Company',
      cardNumber: 'ID Number ',
      haveOrNotPhotos: 'With or without photos',
      status: 'State',
      photo: 'Photo',
      signUp: 'sign up',
      uploadPortrait: 'Upload Portrait',
      uploadPortraitPlaceholder: 'Please upload a portrait',
      uploaded: 'Uploaded',
      noUpload: 'Not uploaded',
      addCard: 'Add new documents',
      editCard: 'Edit ID information',
      gender: 'Gender',
      position: 'Position',
      identityCardType: 'Type of ID document',
      mail: 'Email',
      country: 'Country/Region',
      submitForm: 'Submit Form ',
      group: 'Delegation',
      editCardInfo: 'Edit ID information',
      applyCard: 'Applying for documents',
      IDNumberError: 'Please enter the correct ID number'
    },
    batch: {
      uploadList: 'Upload List',
      uploadPortrait: 'Upload Portrait',
      downloadTemplate: 'Download Template',
      selectTemplate: 'Select Template',
      confirmSubmit: 'Confirm submission',
      unselectedFile: 'No file selected',
      uploadHistory: 'Historical upload records',
      operationAccount: 'Operation account',
      uploadTime: 'Upload time',
      uploadResult: 'Upload Results',
      clickToUpload: 'Click to upload',
      clear: 'clean up',
      selectPicture: 'Select Picture',
      selectPictureTips:
        '{selected} files selected, waiting to upload {unselected} files',
      downloadErrorMessage: 'Download error message',
      uploadTotal: 'Total number of uploads:',
      successTotal: 'Total Success:',
      errorTotal: 'Total number of failures:',
      unselectedFileTips: 'Please select a file and upload it again',
      unselectedPhotoTips: 'Please select an image and upload it again',
      removePhoto: 'Remove Picture',
      desc: 'explain',
      desc1:
        "The photo name must be exactly the same as the applicant's ID number (ID number, passport number, etc.). If there is any difference, the image upload will fail.",
      desc2:
        'The electronic photo file is named after the ID number or ID card number. The pixel is not less than 500 * 700, the size is 240px * 300px, and the photo size is within 200KB to 400KB. It supports jpg and other formats.',
      desc3:
        'The portrait must be a standard ID photo with a white background, with a front face and a face size of at least one-third of the entire photo. The facial expression should be normal, and sunglasses should not be worn. Multiple faces are not allowed.',
      photoExample: 'Example portrait photo:',
      typeError:
        '{name} is automatically removed from the list because it is not an image/jpeg or image/png image',
      sizeError:
        'The size of {name} exceeds the 2MB limit and is automatically deleted from the list'
    }
  }
}
