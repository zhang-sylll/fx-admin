export const download = {
  success: '导出成功',
  successPrompt: '可在浏览器下载任务中查看',
  error: '导出失败',
  errorPrompt: '请重试或与管理员联系',
  operateSuccess: '操作成功',
  exporting: '正在导出，请稍候...',
  unknownException: '未知异常, 请截图与管理员联系'
}
