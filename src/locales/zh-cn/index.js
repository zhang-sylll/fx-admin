import { download } from './download'

export default {
  download,
  group: {
    refresh: '刷新',
    delegation: '代表团',
    contactName: '联系人',
    contactNumber: '联系人电话',
    account: '账号',
    password: '密码',
    belongsDelegation: '所属代表团',
    firstLevelDelegation: '一级代表团',
    secondLevelDelegation: '二级代表团'
  },
  global: {
    delegationManagement: '代表团管理',
    exportTooltip: '导出的条件与列表查询条件一致',
    validUploadLimit: `上传失败, 文件最大数量为 {limit}个, 已上传 {uploadedLength} 个`,
    vaildUploadSize: `上传失败, 文件大小上限为 {size}MB, 所选文件为 {userSize}MB`,
    vaildUploadType: '系统目前只支持上传图片',
    uploadTip: '请上传小于 {size}MB 的 {type} 文件',
    operation: '操作',
    edit: '编辑',
    delete: '删除',
    search: '查询',
    create: '新增',
    export: '导出',
    import: '导入',
    index: '序号',
    dargFileHereOr: '拖拽文件至此或',
    clickToUpload: '点击上传',
    file: '文件',
    logOut: '退出登录',
    deleteTips: '删除{tips}，是否继续',
    tips: '提示',
    confirm: '确认',
    cancel: '取消',
    man: '男',
    woman: '女',
    gender: '性別',
    pleaseInput: '请输入{label}',
    pleaseInputCorrect: '请输入正确的{label}',
    pleaseSelect: '请选择{label}',
    pleaseUpload: '请上传{label}',
    apply: '申请',
    myApply: '我的申请',
    cardApply: '证件申请'
  },
  login: {
    login: '登录',
    account: '账号',
    accountPlaceholder: '请输入账号',
    password: '密码',
    passwordPlaceholder: '请输入密码'
  },
  cardApply: {
    myApply: {
      cardInfo: '证件信息',
      pageTitle: '我的申请列表',
      name: '姓名',
      phone: '手机',
      cardType: '证件类型',
      company: '公司名称',
      cardNumber: '证件号码',
      haveOrNotPhotos: '有无照片',
      status: '状态',
      photo: '照片',
      signUp: '报名',
      uploadPortrait: '上传人像',
      uploadPortraitPlaceholder: '请上传人像',
      uploaded: '已上传',
      noUpload: '未上传',
      addCard: '新增证件',
      editCard: '编辑证件信息',
      gender: '性别',
      position: '职位',
      identityCardType: '身份证件类型',
      mail: '邮箱',
      country: '国家/地区',
      submitForm: '提交表单',
      group: '代表团',
      editCardInfo: '编辑证件信息',
      applyCard: '申请证件',
      IDNumberError: '请输入正确的身份证号码'
    },
    batch: {
      uploadList: '上传名单',
      uploadPortrait: '上传人像',
      downloadTemplate: '下载模板',
      selectTemplate: '选择模板',
      confirmSubmit: '确认提交',
      unselectedFile: '未选择文件',
      uploadHistory: '历史上传记录',
      operationAccount: '操作账号',
      uploadTime: '上传时间',
      uploadResult: '上传结果',
      clickToUpload: '点击上传',
      clear: '清除',
      selectPicture: '选择图片',
      selectPictureTips:
        '已选中 {selected} 个文件， 待上传 {unselected} 个文件',
      downloadErrorMessage: '下载错误信息',
      uploadTotal: '上传总数：',
      successTotal: '成功总数：',
      errorTotal: '失败总数：',
      unselectedFileTips: '请选择文件再上传',
      unselectedPhotoTips: '请选择图片再上传',
      removePhoto: '移除图片',
      desc: '说明',
      desc1:
        '照片名称必须与申请人的证件号码（身份证号、护照号等）完全相同，如有不同将导致图片上传失败。',
      desc2:
        '电子照片文件以身份证号或证件号命名，像素不低于500*700，尺寸为240px * 300px，照片大小在200KB至400KB以内，支持jpg等格式。',
      desc3:
        '人像必须为白底标准证件照，正脸，人脸大小占整张照片1/3以上，，表情正常，不可带墨镜，不可出现多人脸。',
      photoExample: '实例人像照片：',
      typeError: '{name}不是image/jpeg、image/png类型图片，自动从列表中移除',
      sizeError: '{name}的大小超过2MB的限制，自动从列表中移除'
    }
  }
}
